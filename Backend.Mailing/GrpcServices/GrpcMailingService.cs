﻿using Backend.Infrastructure;
using Backend.Infrastructure.Services.Interfaces;
using Grpc.Core;

namespace Backend.Mailing.GrpcServices;

public class GrpcMailingService : MailingService.MailingServiceBase
{
    private readonly ILogger<GrpcMailingService> _logger;
    private readonly IMailService _mailService;

    public GrpcMailingService(ILogger<GrpcMailingService> logger, IMailService mailService)
    {
        _logger = logger;
        _mailService = mailService;
    }

    public override async Task<MailReply> SendMail(MailRequest request, ServerCallContext context)
    {
        _logger.LogInformation("Received to send: {} {} \"{}\"", request.Email, request.Subject, request.Message);

        await _mailService.SendMail(request.Email, request.Subject, request.Message);

        return new MailReply()
        {
            IsSuccess = true
        };
    }
}
