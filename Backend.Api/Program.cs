using Backend.Infrastructure.Extensions;

namespace Backend.Api;

public class Program
{
    public static void Main(string[] args)
    {
        WebApplication.CreateBuilder(args)
            .AddGeneralDependencies()
            .AddBackendServices()
            .AddWebAppDatabase()
            .AddRemoteMailService()
            .Build()
            .AddGeneralDependencies()
            .Run();
    }
}
