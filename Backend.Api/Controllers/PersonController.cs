﻿using Backend.Core.DataTransferObjects;
using Backend.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class PersonController : ControllerBase
{
    private readonly IPersonService _personService;

    public PersonController(IPersonService personService)
    {
        _personService = personService;
    }

    [HttpPost]
    public async Task<ActionResult> Add([FromBody] PersonAddDTO person)
    {
        await _personService.AddPerson(person);

        return NoContent();
    }

    [HttpPut]
    public async Task<ActionResult> Update([FromBody] PersonUpdateDTO person)
    {
        await _personService.UpdatePerson(person);

        return NoContent();
    }

    [HttpGet]
    public async Task<ActionResult<List<PersonDTO>>> GetAll()
    {
        return Ok(await _personService.GetPersons());
    }

    [HttpGet("{id:guid}")]
    public async Task<ActionResult<PersonDTO>> GetById([FromRoute] Guid id)
    {
        return Ok(await _personService.GetPerson(id));
    }

    [HttpDelete("{id:guid}")]
    public async Task<ActionResult<PersonDTO>> Delete([FromRoute] Guid id)
    {
        await _personService.DeletePerson(id);

        return NoContent();
    }
}
