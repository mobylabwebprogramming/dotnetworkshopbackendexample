﻿using Backend.Core.BaseObjects;

namespace Backend.Core.Entities;

public class ClassActivity : BaseEntity
{
    public string Title { get; set; } = default!;
    public string? Description { get; set; }
    public DateTime Date { get; set; }
    public Guid PersonId { get; set; }

    public Person Person { get; set; } = default!;
}
