﻿using Backend.Core.BaseObjects;

namespace Backend.Core.Entities;

public class Person : BaseEntity
{
    public string FirstName { get; set; } = default!;
    public string LastName { get; set; } = default!;
    public string? Initials { get; set; }
    public int Age { get; set; }

    public ICollection<ClassActivity> ClassActivities { get; set; } = default!;
}
