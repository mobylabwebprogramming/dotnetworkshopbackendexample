﻿namespace Backend.Core.Configurations;

public class RemoteMailConfiguration
{
    public string ServiceAddress { get; set; } = default!;
}
