﻿namespace Backend.Core.Configurations;

public class MailConfiguration
{
    public string MailHost { get; set; } = default!;
    public int MailPort { get; set; }
    public string MailAddress { get; set; } = default!;
    public string MailUser { get; set; } = default!;
    public string MailPassword { get; set; } = default!;
}
