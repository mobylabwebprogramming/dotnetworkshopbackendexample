﻿using Backend.Core.Configurations;
using Backend.Infrastructure.Database;
using Backend.Infrastructure.Services.Implementations;
using Backend.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Backend.Infrastructure.Extensions;

public static class WebApplicationBuilderExtensions
{
    private const string WebAppDatabaseConnection = "BackendDatabase";

    public static WebApplicationBuilder AddWebAppDatabase(this WebApplicationBuilder builder)
    {
        builder.Services.AddDbContext<WebAppDatabaseContext>(opt =>
            opt.UseNpgsql(builder.Configuration.GetConnectionString(WebAppDatabaseConnection)));

        return builder;
    }

    public static WebApplicationBuilder AddGeneralDependencies(this WebApplicationBuilder builder)
    {
        builder.Services.AddControllers();
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        return builder;
    }

    public static WebApplicationBuilder AddBackendServices(this WebApplicationBuilder builder)
    {
        builder.Services.AddScoped<IPersonService, PersonService>();

        return builder;
    }

    public static WebApplicationBuilder AddMailService(this WebApplicationBuilder builder)
    {
        builder.Services.Configure<MailConfiguration>(builder.Configuration.GetSection(nameof(MailConfiguration)));
        builder.Services.AddScoped<IMailService, MailService>();

        return builder;
    }

    public static WebApplicationBuilder AddRemoteMailService(this WebApplicationBuilder builder)
    {
        builder.Services.Configure<RemoteMailConfiguration>(builder.Configuration.GetSection(nameof(RemoteMailConfiguration)));
        builder.Services.AddScoped<IMailService, RemoteMailService>();

        return builder;
    }

    public static WebApplication AddGeneralDependencies(this WebApplication app)
    {
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();
        app.UseAuthorization();
        app.MapControllers();

        return app;
    }
}
