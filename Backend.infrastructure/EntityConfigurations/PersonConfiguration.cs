﻿using Backend.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.Infrastructure.EntityConfigurations;

public class PersonConfiguration : IEntityTypeConfiguration<Person>
{
    public void Configure(EntityTypeBuilder<Person> builder)
    {
        builder.HasKey(e => e.Id);
        builder.Property(e => e.FirstName)
            .IsRequired()
            .HasMaxLength(255);
        builder.Property(e => e.LastName)
            .IsRequired()
            .HasMaxLength(255);
        builder.Property(e => e.Initials)
            .IsRequired(false)
            .HasMaxLength(255);
        builder.Property(e => e.Age)
            .HasDefaultValue(18);
        builder.Property(e => e.CreatedAt)
            .IsRequired();
        builder.Property(e => e.UpdatedAt)
            .IsRequired();
    }
}
