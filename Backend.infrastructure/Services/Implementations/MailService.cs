﻿using Backend.Core.Configurations;
using Backend.Infrastructure.Services.Interfaces;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;

namespace Backend.Infrastructure.Services.Implementations;

public class MailService : IMailService
{
    private readonly MailConfiguration _mailConfiguration;

    public MailService(IOptions<MailConfiguration> mailConfiguration)
    {
        _mailConfiguration = mailConfiguration.Value;
    }

    public async Task SendMail(string email, string subject, string message)
    {
        var messageToSend = new MimeMessage();
        messageToSend.From.Add(new MailboxAddress("Backend", _mailConfiguration.MailAddress));
        messageToSend.To.Add(new MailboxAddress(email, email));
        messageToSend.Subject = subject;
        messageToSend.Body = new TextPart("plain")
        {
            Text = message
        };

        using var client = new SmtpClient();

        await client.ConnectAsync(_mailConfiguration.MailHost, _mailConfiguration.MailPort);
        await client.AuthenticateAsync(_mailConfiguration.MailUser, _mailConfiguration.MailPassword);
        await client.SendAsync(messageToSend);
        await client.DisconnectAsync(true);
    }
}
