﻿using Backend.Core.Configurations;
using Backend.Infrastructure.Services.Interfaces;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Backend.Infrastructure.Services.Implementations;

public class RemoteMailService : IMailService
{
    private readonly ILogger<RemoteMailService> _logger;
    private readonly RemoteMailConfiguration _remoteMailConfiguration;

    public RemoteMailService(ILogger<RemoteMailService> logger, 
        IOptions<RemoteMailConfiguration> remoteMailConfiguration)
    {
        _logger = logger;
        _remoteMailConfiguration = remoteMailConfiguration.Value;
    }

    public async Task SendMail(string email, string subject, string message)
    {
        _logger.LogInformation("Sending to remote service: {} {} \"{}\"", email, subject, message);
        
        using var channel = GrpcChannel.ForAddress(_remoteMailConfiguration.ServiceAddress);
        var client = new MailingService.MailingServiceClient(channel);

        var response = await client.SendMailAsync(new MailRequest()
        {
            Email = email,
            Subject = subject, 
            Message = message
        });

        _logger.LogInformation("Responded with success: {}", response.IsSuccess);
    }
}
