﻿using System.Linq.Expressions;
using Backend.Core.DataTransferObjects;
using Backend.Core.Entities;
using Backend.Infrastructure.Database;
using Backend.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Backend.Infrastructure.Services.Implementations;

public class PersonService : IPersonService
{
    private readonly ILogger<PersonService> _logger;
    private readonly IMailService _mailService;
    private readonly WebAppDatabaseContext _databaseContext;

    private static readonly Expression<Func<Person, PersonDTO>> Projection = e => new()
    {
        Id = e.Id,
        FirstName = e.FirstName,
        Initials = e.Initials,
        LastName = e.LastName,
        CreatedAt = e.CreatedAt,
        UpdatedAt = e.UpdatedAt
    };

    public PersonService(WebAppDatabaseContext databaseContext, ILogger<PersonService> logger, IMailService mailService)
    {
        _databaseContext = databaseContext;
        _logger = logger;
        _mailService = mailService;
    }

    public async Task AddPerson(PersonAddDTO person)
    {
        _logger.LogInformation("Adding the following person: {} {} {}", person.FirstName, person.Initials, person.LastName);

        await _databaseContext.AddAsync(new Person
        {
            FirstName = person.FirstName,
            LastName = person.LastName,
            Initials = person.Initials,
            Age = person.Age
        });

        await _databaseContext.SaveChangesAsync();

        await _mailService.SendMail("test@gmail.com", "New person added", 
            $"A new person named {person.FirstName} {person.LastName} was added");
    }

    public async Task UpdatePerson(PersonUpdateDTO person)
    {
        var entry = await _databaseContext.Set<Person>().FirstAsync(e => e.Id == person.Id);

        entry.FirstName = person.FirstName;
        entry.LastName = person.LastName;
        entry.Initials = person.Initials;
        entry.Age = person.Age;
        entry.UpdateTime();

        await _databaseContext.SaveChangesAsync();
    }

    public async Task<List<PersonDTO>> GetPersons()
    {
        return await _databaseContext.Set<Person>().Select(Projection).ToListAsync();
    }

    public async Task<PersonDTO> GetPerson(Guid id)
    {
        return await _databaseContext.Set<Person>().Select(Projection).FirstAsync(e => e.Id == id);
    }

    public async Task DeletePerson(Guid id)
    {
        var entry = await _databaseContext.Set<Person>().FirstAsync(e => e.Id == id);

        _databaseContext.Remove(entry);

        await _databaseContext.SaveChangesAsync();
    }
}
