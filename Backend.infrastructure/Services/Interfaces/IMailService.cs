﻿namespace Backend.Infrastructure.Services.Interfaces;

public interface IMailService
{
    public Task SendMail(string email, string subject, string message);
}
