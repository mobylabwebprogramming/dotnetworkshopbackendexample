﻿using Backend.Core.DataTransferObjects;

namespace Backend.Infrastructure.Services.Interfaces;

public interface IPersonService
{
    public Task AddPerson(PersonAddDTO person);
    public Task UpdatePerson(PersonUpdateDTO person);
    public Task<List<PersonDTO>> GetPersons();
    public Task<PersonDTO> GetPerson(Guid id);
    public Task DeletePerson(Guid id);
}
