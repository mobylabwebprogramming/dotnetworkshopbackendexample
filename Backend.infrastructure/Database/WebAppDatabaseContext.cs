﻿using Ardalis.EFCore.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Backend.Infrastructure.Database;

public sealed class WebAppDatabaseContext : DbContext
{
    public WebAppDatabaseContext(DbContextOptions<WebAppDatabaseContext> builder) : base(builder)
    {
        Database.Migrate();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.ApplyAllConfigurationsFromCurrentAssembly();
    }
}
